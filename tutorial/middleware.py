import re

from django.conf import settings
from django.shortcuts import redirect
from django.contrib.auth import logout
from django.urls import reverse
EXEMPT_URLS = [re.compile(settings.LOGIN_URL.lstrip('/'))]
if hasattr(settings, 'LOGIN_EXEMPT_URLS'):
    EXEMPT_URLS += [re.compile(url) for url in settings.LOGIN_EXEMPT_URLS]


class LoginRequiredMiddleware:

    def __init__(self, get_response):
        # One-time configuration and initialization.
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return response

    def process_view(self, request, view_func, view_args, view_kwargs):
        assert hasattr(request, 'user')
        path = request.path_info.lstrip('/')
        # this is to log the user out from the middleware
        print(path)
        if path == reverse('logout').lstrip('/'):
            logout(request)
        url_is_exempt = any(url.match(path) for url in EXEMPT_URLS)
        # checking if the user is logged in and trying to get to the exempt page
        if request.user.is_authenticated and url_is_exempt:
            return redirect(settings.LOGIN_REDIRECT_URL)
        # if user is logged in and the user does not access the exempted page
        elif request.user.is_authenticated or url_is_exempt:
            return None
        else:
            # if user is not logged in then go to login url
            return redirect(settings.LOGIN_URL)
