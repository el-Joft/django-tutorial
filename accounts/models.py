from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    description = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    website = models.URLField(max_length=200, blank=True, null=True)
    phone = models.IntegerField(default=0)
    email_confirmed = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = 'Profiles'


@receiver(post_save, sender=User)
def create_profile(sender, instance, created, **Kwargs):
    # if Kwargs['create']:
    #   user_profile = UserProfile.objects.create(user=Kwargs['instance'])
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
