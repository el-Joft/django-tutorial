from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.contrib.auth.models import User
from django import forms
from django.core.exceptions import ValidationError


class RegistrationForm(forms.ModelForm):
    username = forms.CharField(max_length=128, help_text="", widget=forms.TextInput(
        attrs={'class': 'validate', 'placeholder': 'Username', 'required': 'required'}))
    first_name = forms.CharField(max_length=128, help_text="", widget=forms.TextInput(
        attrs={'class': 'validate', 'placeholder': 'First Name', 'required': 'required'}))
    last_name = forms.CharField(max_length=128, help_text="", widget=forms.TextInput(
        attrs={'class': 'validate', 'placeholder': 'Last Name', 'required': 'required'}))
    email = forms.EmailField(max_length=50, help_text="", widget=forms.EmailInput(
        attrs={'class': 'validate', 'placeholder': 'example@email.com', 'required': 'required'}))
    password = forms.CharField(min_length=8, max_length=20, help_text="", widget=forms.PasswordInput(
        attrs={'class': 'validate', 'placeholder': 'password', 'required': 'required'}))
    password2 = forms.CharField(label='Password confirmation', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password')

    # checks if username already exist
    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise ValidationError("Username already exists")
        return username

    # check if email has been taken
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise ValidationError("Email already exists")
        return email

    def clean_password2(self):
        # Check that the two password entries match
        password1 = self.cleaned_data.get("password")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords don't match")
        return password2

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(RegistrationForm, self).save(commit=False)
        user.first_name = self.cleaned_data["first_name"]
        user.email = self.cleaned_data["email"]
        user.last_name = self.cleaned_data["last_name"]
        # setting commit to false for the application to wait and hash the user password
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class EditProfile(UserChangeForm):
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email')
