from django.shortcuts import render, redirect
# added code for email verification
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.template.loader import render_to_string
from accounts.token import account_activation_token
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.models import User
from django.views.generic.edit import UpdateView
# end of email verification code
from django.contrib.auth.forms import PasswordChangeForm
from accounts.forms import RegistrationForm, EditProfile
from django.contrib import messages
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth import update_session_auth_hash


# Create your views here.
def home(request):
    return render(request, 'accounts/home.html')


# The below view allows user to register and get authenticated
def register(request):
    if request.method == 'POST':
        f = RegistrationForm(request.POST)
        if f.is_valid():
            user = f.save(commit=False)  # meaning don't save yet
            user.is_active = False
            user.save()
            messages.success(request, 'Account created successfully')
            # username = f.cleaned_data.get('username')
            # raw_password = f.cleaned_data.get('password')
            current_site = get_current_site(request)
            subject = 'Activate Your Joftify Account'
            message = render_to_string('accounts/account_activation_email.html', {
                            'user': user,
                            'domain': current_site.domain,
                            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                            'token': account_activation_token.make_token(user),
            })
            user.email_user(subject, message)
            return redirect('/account/account_activation_sent')
            # user = authenticate(username=username, password=raw_password)
            # login(request, user)
            # return redirect('/account')
    else:
        f = RegistrationForm()

    return render(request, 'accounts/register.html', {'form': f})


def profile(request):
    context = {
        'user': request.user
    }
    return render(request, 'accounts/profile.html', context)


def edit_profile(request):
    if request.method == 'POST':
        f = EditProfile(request.POST, instance=request.user)
        if f.is_valid():
            f.save()
            return redirect('/account/profile')
    else:
        f = EditProfile(instance=request.user)
        return render(request, 'accounts/edit_profile.html', {'form': f})


def change_password(request):
    if request.method == 'POST':
        # data=request.POST is used to specify what the user is
        f = PasswordChangeForm(data=request.POST, user=request.user)
        if f.is_valid():
            f.save()
            update_session_auth_hash(request, f.user)
            return redirect('/account/profile')
    else:
        f = PasswordChangeForm(user=request.user)
        return render(request, 'accounts/edit_profile.html', {'form': f})


# def register(request):
#     if request.method == 'POST':
#         f = RegistrationForm(request.POST)
#         if f.is_valid():
#             user = f.save(commit=False)  # meaning don't save yet
#             user.is_active = False
#             user.save()
#             current_site = get_current_site(request)
#             subject = 'Activate Your Joftify Account'
#             message = render_to_string('accounts/account_activation_email.html', {
#                 'user': user,
#                 'domain': current_site.domain,
#                 'uid': urlsafe_base64_encode(force_bytes(user.pk)),
#                 'token': account_activation_token.make_token(user),
#             })
#             user.email_user(subject, message)
#             return redirect('/account/account_activation_sent')
#     else:
#         f = RegistrationForm()
#     return render(request, 'accounts/register.html', {'form': f})


def account_activation_sent(request):
    return render(request, 'accounts/account_activation_sent.html')


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.profile.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('/account/')
    else:
        return render(request, 'accounts/account_activation_invalid.html')


def logout_view(request):
    logout(request)
    return redirect('/account/login/')
